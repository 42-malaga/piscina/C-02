/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_uppercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/24 10:00:46 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/01 13:03:04 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @brief	Checks if a character it's an upper letter.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's an upper letter; 0 otherwise.
*/
int	is_letter_upper(char c)
{
	return ('A' <= c && c <= 'Z');
}

int	ft_str_is_uppercase(char *str)
{
	int	upper;

	upper = 1;
	while (*str && upper)
	{
		if (!is_letter_upper(*str))
			upper = 0;
		str++;
	}
	return (upper);
}
