/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 18:34:39 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/03 23:40:14 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

#define HEX "0123456789abcdef"

/**
 * @brief	Prints the memory address of a pointer in hexadecimal format,
 *          using a determined number of digits padded with 0s if neccesary.
 *
 * @param addr		Memory address to convert.
 * @param length    Number of digits to use.
 */
void	hex(long addr, int length)
{
	char	hex[16];

	hex[length - 1] = '\0';
	while (length--)
	{
		hex[length] = HEX[addr % 16];
		addr /= 16;
	}
	write(1, hex, 16);
}

/**
 * @brief	Prints the address of a pointer in 16 digits format.
 * 			If less than 16 digits, it will be padded with 0s.
 *
 * @param addr	Memory address where the 16-element block starts.
 */
void	print_address(long addr)
{
	hex(addr, 16);
	write(1, ": ", 2);
}

/**
 * @brief   Prints the content of a 16-element block of memory using
 *          2 bytes hexadecimal format, separated by spaces.
 *          If the block has less than 16 elements, it will be padded
 *          with spaces.
 * 
 * @param addr  Memory address where the 16-element block starts.
 * @param size  Number of remaining elements to print.
 */
void	print_bytes(const char *addr, unsigned int size)
{
	int	spaces;
	int	i;

	i = 0;
	while (i < 16 && size - i)
	{
		write(1, &HEX[addr[i] / 16], 1);
		write(1, &HEX[addr[i] % 16], 1);
		if (i % 2)
			write(1, " ", 1);
		i++;
	}
	if (size < 16)
	{
		i = 0;
		spaces = (16 - size) * 2 + (16 - size) / 2;
		while (i++ < spaces)
			write(1, " ", 1);
		if (size % 2)
			write(1, " ", 1);
	}
}

/**
 * @brief	Prints the contents of the addressed memory
 * 			in block of 16 elements and displaying the
 * 			unprintable characters as '.'.
 *
 * @param addr	Memory address where the 16-element block starts.
 */
void	print_content(char *addr, unsigned int size)
{
	char	*actual;
	int		i;

	i = 0;
	actual = addr + i;
	while (actual && i < 16 && size - i)
	{
		if ((' ' <= *actual && *actual <= '~'))
			write(1, actual, 1);
		else
			write(1, ".", 1);
		actual = addr + ++i;
	}
}

void	*ft_print_memory(void *addr, unsigned int size)
{
	unsigned int	i;
	void			*actual;

	i = 0;
	while (i < size)
	{
		actual = addr + i;
		print_address((long) actual);
		print_bytes(actual, size - i);
		print_content(actual, size - i);
		write(1, "\n", 1);
		i += 16;
	}
	return (addr);
}
