/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/25 11:24:13 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/05 19:58:16 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/**
 * @brief	Converts a number to hexadecimal.
 * 
 * @param n		Number to convert.
 */
void	hex(int n)
{
	char	*simbols;

	simbols = "0123456789abcdef";
	write(1, "\\", 1);
	write(1, &simbols[n / 16], 1);
	write(1, &simbols[n % 16], 1);
}

/**
 * @brief	Checks if a character it's printable.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's printable; 0 otherwise.
*/
int	is_printable(char c)
{
	return (' ' <= c && c <= '~');
}

void	ft_putstr_non_printable(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (is_printable(str[i]))
			write(1, &str[i], 1);
		else
			hex(str[i] & 0xff);
		i++;
	}
}
