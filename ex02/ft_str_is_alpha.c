/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_alpha.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/23 12:44:11 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/01 13:02:37 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @brief	Checks if a character it's a letter.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's a letter; 0 otherwise.
*/
int	is_letter(char c)
{
	return (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'));
}

int	ft_str_is_alpha(char *str)
{
	int	alpha;

	alpha = 1;
	while (*str && alpha)
	{
		if (!is_letter(*str))
			alpha = 0;
		str++;
	}
	return (alpha);
}
