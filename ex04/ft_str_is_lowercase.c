/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_lowercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/23 12:44:11 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/01 13:02:55 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @brief	Checks if a character it's a lower letter.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's a lower letter; 0 otherwise.
*/
int	is_letter_lower(char c)
{
	return ('a' <= c && c <= 'z');
}

int	ft_str_is_lowercase(char *str)
{
	int	lower;

	lower = 1;
	while (*str && lower)
	{
		if (!is_letter_lower(*str))
			lower = 0;
		str++;
	}
	return (lower);
}
