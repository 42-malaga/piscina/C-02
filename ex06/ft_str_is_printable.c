/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/24 10:08:49 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/01 13:03:48 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @brief	Checks if a character it's printable.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's printable; 0 otherwise.
*/
int	is_printable(char c)
{
	return (' ' <= c && c <= '~');
}

int	ft_str_is_printable(char *str)
{
	int	printable;

	printable = 1;
	while (*str && printable)
	{
		if (!is_printable(*str))
			printable = 0;
		str++;
	}
	return (printable);
}
