#include <stdio.h>
#include <string.h>

void ft_putstr_non_printable(char *str);

int main()
{
	char a[] = "Coucou\ttu vas bien ?";
	char b[] = {-32, -57, 3, 65, 97, 0};
	char c[] = {-32, -57, 0, 3, 65, 97};
	char d[] = "Hólâ a töd_s, ¿cómo estáis?";

	printf("a: \"%s\"\n", a);
	printf("b: \"%s\"\n", b);
	printf("c: \"%s\"\n", c);
	printf("d: \"%s\"\n\n", d);

	puts("a: ");
	ft_putstr_non_printable(a);
	puts("\n");
	
	puts("b: ");
	ft_putstr_non_printable(b);
	puts("\n");
	
	puts("c: ");
	ft_putstr_non_printable(c);
	puts("\n");

	puts("d: ");
	ft_putstr_non_printable(d);
	puts("\n");

	return (0);
}
