#include <stdio.h>

int	ft_str_is_numeric(char *str);

int main()
{
	char *a = "Hola Mundo.";
	char *b = "HolaMundo";
	char *c = "1234567890";
	char *d = "123A5G7890";
	char *e = "";

	printf("a: %d\t\"%s\"\n", ft_str_is_numeric(a), a);
	printf("b: %d\t\"%s\"\n", ft_str_is_numeric(b), b);
	printf("c: %d\t\"%s\"\n", ft_str_is_numeric(c), c);
	printf("d: %d\t\"%s\"\n", ft_str_is_numeric(d), d);
	printf("e: %d\t\"%s\"\n", ft_str_is_numeric(e), e);
}
