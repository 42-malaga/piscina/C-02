/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex08.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edbander <edbander@student.42malaga.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/27 10:39:38 by edbander          #+#    #+#             */
/*   lowdated: 2022/10/27 18:13:17 by edbander         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);

int main()
{
	char src[] = "Hola Mundo";
	char dst[] = "HOLA MUNDO";
	unsigned int size = 5;
	
	printf("src : \"%s\"\n", src);
	printf("dst : \"%s\"\n", dst);
	printf("size: %d\n\n", size);
	
	// printf("strlcpy   : %u\n", strlcpy(dst, src, size));
	printf("ft_strlcpy: %d\n\n", ft_strlcpy(dst, src, size));

	printf("src : \"%s\"\n", src);
	printf("dst : \"%s\"\n", dst);
	printf("---------------------\n");

	char src2[] = "Hola Mundo";
	char dst2[] = "";
	unsigned int size2 = 7;

	printf("src : \"%s\"\n", src2);
	printf("dst : \"%s\"\n", dst2);
	printf("size: %d\n\n", size2);

	// printf("strlcpy   : %u\n", strlcpy(dst2, src2, size2));
	printf("ft_strlcpy: %d\n\n", ft_strlcpy(dst2, src2, size2));

	printf("src : \"%s\"\n", src2);
	printf("dst : \"%s\"\n", dst2);
	printf("---------------------\n");

	char src3[] = "Hola Mundo";
	char dst3[] = "HOLA MUNDO";
	unsigned int size3 = 0;

	printf("src : \"%s\"\n", src3);
	printf("dst : \"%s\"\n", dst3);
	printf("size: %d\n\n", size3);

	// printf("strlcpy   : %u\n", strlcpy(dst3, src3, size3));
	printf("ft_strlcpy: %d\n\n", ft_strlcpy(dst3, src3, size3));

	printf("src : \"%s\"\n", src3);
	printf("dst : \"%s\"\n", dst3);
	printf("---------------------\n");

	char src4[] = "Hola";
	char dst4[] = "HOLA MUNDO";
	unsigned int size4 = 7;

	printf("src : \"%s\"\n", src4);
	printf("dst : \"%s\"\n", dst4);
	printf("size: %d\n\n", size4);

	// printf("strlcpy   : %u\n", strlcpy(dst4, src4, size4));
	printf("ft_strlcpy: %d\n\n", ft_strlcpy(dst4, src4, size4));

	printf("src : \"%s\"\n", src4);
	printf("dst : \"%s\"\n", dst4);

	return (0);
}
