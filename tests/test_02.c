#include <stdio.h>

int	ft_str_is_alpha(char *str);

int main()
{
	char *a = "Hola Mundo.";
	char *b = "HolaMundo";
	char *c = "";

	printf("a: %d\t\"%s\"\n", ft_str_is_alpha(a), a);
	printf("b: %d\t\"%s\"\n", ft_str_is_alpha(b), b);
	printf("c: %d\t\"%s\"\n", ft_str_is_alpha(c), c);
}
