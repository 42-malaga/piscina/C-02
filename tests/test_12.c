#include <stdio.h>
#include <stdlib.h>

void 	*ft_print_memory(void *addr, unsigned int size);

int	main(void)
{
	char a[200] = "Bonjour les aminches\t\n\tc\a est fou\ttout\tce qu on peut faire avec\t\n\tprint_memory\n\n\n\tlol.lol\n \0";
	char b[] = {57, 64, 98, 111, 10, 9, 5, 127, -128, -2, -44, 123, 34, 45, 102, -102, 0};
	int	c[] = {4, 22, 10, 8, 9, 0, 34, 16, 4533, 35};
	char d[] = "Hólâ a töd_s, ¿cómo estáis?";

	printf("a: \"%s\"\n", a);
	ft_print_memory(&a, 75);
	printf("\n");

	printf("b: \"%s\"\n", b);
	ft_print_memory(&b, 17);
	printf("\n");

	printf("c: (números)\n");
	ft_print_memory(c, 10);
	printf("\n");

	printf("d: \"%s\"\n", d);
	ft_print_memory(&d, 30);
	printf("\n");

	return (0);
}
