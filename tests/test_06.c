#include <stdio.h>

int	ft_str_is_printable(char *str);

int main()
{
	char a[] = "A@`f'////\\SDaaSD";
	char b[] = "Hola\tMundo";
	char c[] = "\1uejejdnfm42ncAS";
	char d[] = "";

	printf("a: \"%s\"\n", a);	ft_strupcase(a);	printf("a: \"%s\"\n\n", a);
	printf("b: \"%s\"\n", b);	ft_strupcase(b);	printf("b: \"%s\"\n\n", b);
	printf("c: \"%s\"\n", c);	ft_strupcase(c);	printf("c: \"%s\"\n\n", c);
	printf("d: \"%s\"\n", d);	ft_strupcase(d);	printf("d: \"%s\"\n\n", d);
}
