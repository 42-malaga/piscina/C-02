#include <stdio.h>

int	ft_strlowcase(char *str);

int main()
{
	char a[] = "A@`f'////\\SDaaSD";
	char b[] = "Hola\tMundo";
	char c[] = "\1uejejdnfm42ncAS";
	char d[] = "";

	printf("a: \"%s\"\n", a);	ft_strlowcase(a);	printf("a: \"%s\"\n\n", a);
	printf("b: \"%s\"\n", b);	ft_strlowcase(b);	printf("b: \"%s\"\n\n", b);
	printf("c: \"%s\"\n", c);	ft_strlowcase(c);	printf("c: \"%s\"\n\n", c);
	printf("d: \"%s\"\n", d);	ft_strlowcase(d);	printf("d: \"%s\"\n\n", d);
}
