#include <stdio.h>

int	ft_str_is_uppercase(char *str);

int main()
{
	char *a = "HOLA MUNDO.";
	char *b = "HOLAMUNDO";
	char *c = "1234567890";
	char *d = "123A5G7890";
	char *e = "";

	printf("a: %d\t\"%s\"\n", ft_str_is_uppercase(a), a);
	printf("b: %d\t\"%s\"\n", ft_str_is_uppercase(b), b);
	printf("c: %d\t\"%s\"\n", ft_str_is_uppercase(c), c);
	printf("d: %d\t\"%s\"\n", ft_str_is_uppercase(d), d);
	printf("e: %d\t\"%s\"\n", ft_str_is_uppercase(e), e);
}
