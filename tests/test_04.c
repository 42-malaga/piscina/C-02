#include <stdio.h>

int	ft_str_is_lowercase(char *str);

int main()
{
	char *a = "hola mundo.";
	char *b = "holamundo";
	char *c = "1234567890";
	char *d = "123A5G7890";
	char *e = "";

	printf("a: %d\t\"%s\"\n", ft_str_is_lowercase(a), a);
	printf("b: %d\t\"%s\"\n", ft_str_is_lowercase(b), b);
	printf("c: %d\t\"%s\"\n", ft_str_is_lowercase(c), c);
	printf("d: %d\t\"%s\"\n", ft_str_is_lowercase(d), d);
	printf("e: %d\t\"%s\"\n", ft_str_is_lowercase(e), e);
}
