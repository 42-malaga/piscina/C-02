/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlowcase.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/24 18:04:56 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/01 13:06:49 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @brief	Checks if a character it's a upper letter.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's a upper letter; 0 otherwise.
*/
int	is_letter_upper(char c)
{
	return ('A' <= c && c <= 'Z');
}

char	*ft_strlowcase(char *str)
{
	while (*str)
	{
		if (is_letter_upper(*str))
			*str += 32;
		str++;
	}
	return (str);
}
