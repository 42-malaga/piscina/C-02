/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/25 09:47:33 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/05 00:56:09 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

/**
 * @brief	Checks if a character it's a lower letter.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's a lower letter; 0 otherwise.
*/
int	is_letter_lower(char c)
{
	return ('a' <= c && c <= 'z');
}

/**
 * @brief	Checks if a character it's a upper letter.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's a upper letter; 0 otherwise.
*/
int	is_letter_upper(char c)
{
	return ('A' <= c && c <= 'Z');
}

/**
 * @brief	Checks if a character it's a letter or a number (alphanumeric).
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's alphanumeric; 0 otherwise.
*/
int	is_alpha(char c)
{
	return (is_letter_lower(c) || is_letter_upper(c) || ('0' <= c && c <= '9'));
}

/**
 * @brief	Lowercase a letter. 
 * 
 * @param *c	Character to lowercase.
 */
void	to_lower(char *c)
{
	if (is_letter_upper(*c))
		*c += 32;
}

char	*ft_strcapitalize(char *str)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (str[j] != '\0' || str[j + 1] == '\0')
	{
		if (is_alpha(str[i]))
		{
			if (is_alpha(str[j]))
				j++;
			else if (is_letter_lower(str[i]))
				str[i] -= 32;
			else
				i = j;
		}
		else
		{
			i++;
			j = i;
		}
		to_lower(&str[j]);
	}
	return (str);
}
