/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strupcase.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/24 10:15:04 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/01 13:01:04 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @brief	Checks if a character it's a lower letter.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's a lower letter; 0 otherwise.
*/
int	is_letter_lower(char c)
{
	return ('a' <= c && c <= 'z');
}

char	*ft_strupcase(char *str)
{
	while (*str)
	{
		if (is_letter_lower(*str))
			*str -= 32;
		str++;
	}
	return (str);
}
