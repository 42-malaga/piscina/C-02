/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_numeric.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/23 12:44:11 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/01 13:02:46 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * @brief	Checks if a character it's a number.
 * 
 * @param c		Character to be checked.
 * 
 * @return	1 if it's a number; 0 otherwise.
*/
int	is_number(char c)
{
	return ('0' <= c && c <= '9');
}

int	ft_str_is_numeric(char *str)
{
	int	num;

	num = 1;
	while (*str && num)
	{
		if (!is_number(*str))
			num = 0;
		str++;
	}
	return (num);
}
